import React from "react";
import "./App.css";

function test(Com) {
  return class extends React.Component {
    render() {
      return (
        <div className="container">
          <Com></Com>
        </div>
      );
    }
  };
}

@test
class App extends React.Component {
  render() {
    return <div>app</div>;
  }
}

export default App;
